package api

type DeploymentRef struct {
	AppName string `json:"appName"`
}

type EnvironmentVariable struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type Deployment struct {
	DeploymentRef                       // AppName is promoted to Deployment
	ImageUri      string                `json:"imageUri"`
	Port          uint16                `json:"port"`
	Expose        bool                  `json:"expose"`
	Environ       []EnvironmentVariable `json:"environ"`
	Volumes       []string              `json:"volumes"`
}

type DeploymentDetails struct {
	Uri string `json:"uri"`
}

func (d DeploymentDetails) Print() {
	d.Uri = "https://" + d.Uri
	print(d)
}

func (d DeploymentRef) Print() {
	print(d)
}
