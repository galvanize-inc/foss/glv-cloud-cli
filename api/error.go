package api

import (
	"fmt"
)

type ErrorApi struct {
	ErrorMessage string `json:"error"`
}

func (e ErrorApi) Print() {
	fmt.Println(e.ErrorMessage)
}
