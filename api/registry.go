package api

type RegistryRef struct {
	Server string `json:"server"`
}

type Registry struct {
	RegistryRef        // AppName is promoted to Deployment
	Username    string `json:"username"`
	Password    string `json:"password"`
	Email       string `json:"email"`
}
