package api

import (
	"fmt"
	"strconv"

	"github.com/fatih/color"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/utils"
)

type DeploymentExtended struct {
	Deployment Deployment `json:"deployment"`
	Url        string     `json:"url"`
}

type ListApi struct {
	TeamName    string               `json:"teamName"`
	Deployments []DeploymentExtended `json:"deployments"`
}

func (l ListApi) Print() {

	sb := utils.StringBuilder{}
	sb.Append(0, color.CyanString, "Applications", "\n")
	for _, deployment := range l.Deployments {
		sb.AppendSimple(1, "- ")

		sb.Append(0, color.CyanString, "Application Name",
			deployment.Deployment.AppName, "\n")

		sb.Append(2, color.CyanString, "Image URI",
			deployment.Deployment.ImageUri, "\n")

		sb.Append(2, color.CyanString, "Port",
			fmt.Sprintf("%d", deployment.Deployment.Port), "\n")

		sb.Append(2, color.CyanString, "Exposed to Web",
			strconv.FormatBool(deployment.Deployment.Expose), "\n")

		if len(deployment.Deployment.Environ) > 0 {
			sb.Append(2, color.CyanString, "Environment Variables", "\n")
			for _, env := range deployment.Deployment.Environ {
				sb.Append(3, color.CyanString, env.Key, env.Value, "\n")
			}
		}
		if len(deployment.Deployment.Volumes) > 0 {
			sb.Append(2, color.CyanString, "Volumes", "\n")
			for _, volume := range deployment.Deployment.Volumes {
				sb.AppendSimple(3, "- ", volume, "\n")
			}
		}

		webLink := deployment.Url
		if deployment.Deployment.Expose {
			webLink = "https://" + deployment.Url
		}
		sb.Append(2, color.CyanString, "URL", webLink, "\n")
	}

	fmt.Println(sb.Output)
}
