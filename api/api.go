package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"gopkg.in/yaml.v3"
)

type IApi interface {
	Print()
}

func print[T IApi](s T) {
	// Marshal the User struct to JSON
	jsonBytes, err := yaml.Marshal(s)
	if err != nil {
		panic(err)
	}

	// Print the JSON string
	fmt.Println(strings.ReplaceAll(string(jsonBytes), "\\n", "\n"))
}

// Returns a copy of payload because it sometimes returns ErrorApi payload
func ParsePayload[T IApi](resp *http.Response, payload T) (IApi, error) {

	// Read response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 200 {
		err = json.Unmarshal(body, &payload)
		if err != nil {
			return nil, err
		}

		return payload, nil
	} else {
		var errorApi ErrorApi
		err = json.Unmarshal(body, &errorApi)
		if err != nil {
			return nil, err
		}
		return errorApi, nil
	}
}
