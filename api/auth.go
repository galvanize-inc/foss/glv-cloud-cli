package api

type AuthResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    uint32 `json:"expires_in"`
	Scope        string `json:"scope"`
	CreatedAt    uint32 `json:"created_at"`
}

func (a AuthResponse) Print() {
	print(a)
}
