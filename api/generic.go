package api

import (
	"fmt"
)

type Message struct {
	Detail string `json:"detail"`
}

func (m Message) Print() {
	fmt.Println(m.Detail)
}
