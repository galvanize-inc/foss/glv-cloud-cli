package api

import "fmt"

type LogsApi struct {
	Logs string `json:"logs"`
}

func (l LogsApi) Print() {
	fmt.Println(l.Logs)
}
