package utils

import "strings"

type StringBuilder struct {
	Output string
}

func (sb *StringBuilder) AppendSimple(tabs int, s ...string) {
	sb.Output += strings.Repeat("  ", tabs)
	for _, str := range s {
		sb.Output += str
	}
}

type StringFunc func(string, ...interface{}) string

func (sb *StringBuilder) Append(tabs int, fn StringFunc, key string, values ...string) {
	sb.Output += strings.Repeat("  ", tabs)
	sb.Output += fn(key + ": ")
	for _, value := range values {
		sb.Output += value
	}
}
