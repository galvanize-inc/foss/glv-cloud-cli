Write-Host "Parameters ${args}"

Invoke-Expression -Command "winget.exe install -e --id ShiningLight.OpenSSL.Light"

# Generate local cert
& "C:\Program Files\OpenSSL-Win64\bin\openssl.exe" req -x509 -sha256 -days 1095 -nodes -newkey rsa:2048 -addext "subjectAltName = DNS:localhost" -addext "certificatePolicies = 1.2.3.4" -subj "/CN=localhost/C=US/ST=CO/L=Denver/OU=Galvanize Cloud/emailAddress=foss@galvanize.com" -keyout "${args}\localhost.key" -out "${args}\localhost.crt"


Import-Certificate -FilePath "${args}\localhost.crt" -CertStoreLocation Cert:\CurrentUser\Root\
