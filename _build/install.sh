#!/bin/bash
# 2023 Galvanize, All rights reserved.

# Make install directory
mkdir -p /usr/local/galvanize

# Generate local cert
openssl req -x509 \
  -sha256 -days 1095 \
  -nodes \
  -newkey rsa:2048 \
  -addext "subjectAltName = DNS:localhost" \
  -addext "certificatePolicies = 1.2.3.4" \
  -subj "/CN=localhost/C=US/ST=CO/L=Denver/OU=Galvanize Cloud/emailAddress=foss@galvanize.com" \
  -keyout localhost.key \
  -out localhost.crt

# copy certs
sudo cp localhost.crt /usr/local/galvanize/localhost.crt
sudo cp localhost.key /usr/local/galvanize/localhost.key

# Install cert
sudo security add-trusted-cert -d -r \
 trustRoot -k /Library/Keychains/System.keychain localhost.crt

# Install cli binary
sudo cp glv-cloud-cli /usr/local/bin
