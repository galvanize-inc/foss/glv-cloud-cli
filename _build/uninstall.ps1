Write-Host "Parameters ${args}"

Import-Certificate -FilePath "${args}\localhost.crt" -CertStoreLocation Cert:\CurrentUser\Root\

$email="E=foss@galvanize.com"
Get-ChildItem Cert:\CurrentUser\Root\ | Where-Object {$_.Subject -match $email} | Remove-Item
