#!/bin/bash
# 2023 Galvanize, All rights reserved.

if [[ -f /usr/local/bin/glv-cloud-cli ]]; then
rm /usr/local/bin/glv-cloud-cli
fi

if [[ -f /usr/local/glv-cloud-cli ]]; then
rm -rf /usr/local/glv-cloud-cli
fi
