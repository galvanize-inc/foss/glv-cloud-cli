package cmd

import (
	"fmt"
	"os"

	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/auth"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/utils"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func init() {
	rootCmd.AddCommand(loginCmd)
}

var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "login to Galvanize Cloud",
	Long:  `login to Galvanize Cloud`,
	RunE: func(cmd *cobra.Command, args []string) error {

		auth.PrintIdentity(token)

		var promptIn string

		// Check if the cert exists
		if _, err := os.Stat(auth.CertFilePath); os.IsNotExist(err) {
			// Create certificate if it doesn't exist
			auth.CreateCertificate()
			fmt.Println("Press enter after closing Firefox...")
			fmt.Scanln(&promptIn)
		}

		// Check if the key exists
		if _, err := os.Stat(auth.CertKeyPath); os.IsNotExist(err) {
			// Create key if it doesn't exist
			auth.CreateCertificate()
			fmt.Println("Press enter after closing Firefox...")
			fmt.Scanln(&promptIn)
		}

		token = auth.FetchUserToken().AccessToken
		viper.Set("Token", token)

		sb := utils.StringBuilder{}
		sb.Append(0, color.CyanString, "Login Token:", token, "\n")

		// Save changes to file
		err := viper.WriteConfig()
		if err != nil {
			return fmt.Errorf("failed to write config file: %w", err)
		}

		sb.Append(0, color.CyanString, "Saved config to", viper.GetViper().ConfigFileUsed(), "\n")

		fmt.Println(sb.Output)

		return nil
	},
}
