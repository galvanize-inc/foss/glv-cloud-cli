package cmd

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/api"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/auth"
	constants "gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/constants"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

var (
	imageName               string
	environmentVariables    []string
	environmentVariablesApi []api.EnvironmentVariable
	port                    uint16
	expose                  bool
	deleteDeployment        bool
	volumes                 []string
)

func init() {
	deployCmd.Flags().StringVarP(&imageName, "image", "i", "",
		`Fully qualified image name. `+
			`e.g. registry.gitlab.com/path/to/image:latest`)

	deployCmd.Flags().StringVarP(&appName, "app", "a", "",
		"Name of application.")
	deployCmd.MarkFlagRequired("app")

	deployCmd.Flags().Uint16VarP(&port, "port", "p", uint16(80),
		"Port to expose (Defaults to 80).")

	deployCmd.Flags().BoolVarP(&expose, "expose", "x", true,
		"Expose to web.")

	deployCmd.Flags().StringSliceVarP(
		&environmentVariables, "environment", "e",
		[]string{},
		"Environ variables in the form VAR=VAL. This may be repeated.")

	deployCmd.Flags().StringSliceVarP(
		&volumes, "volume", "m",
		[]string{},
		"Volumes in the form hostPath:ContainerPath. This may be repeated.")

	deployCmd.Flags().BoolVarP(
		&deleteDeployment, "delete", "d", false, "Delete deployment.")

	rootCmd.AddCommand(deployCmd)
}

func deleteDeploymentFn() api.IApi {
	fmt.Fprintf(os.Stderr, "Deleting Deployment %s...\n", appName)

	deploymentRef := api.DeploymentRef{
		AppName: appName,
	}

	// Encode the struct as JSON
	deploymentJson, err := json.Marshal(deploymentRef)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest("DELETE",
		server+constants.DeployEndpoint,
		bytes.NewBuffer(deploymentJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	req.Header.Add("Content-Type", "application/json")

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	details := api.DeploymentDetails{}
	payload, err := api.ParsePayload(resp, details)
	if err != nil {
		panic(err)
	}

	return payload
}

func createDeploymentFn() api.IApi {
	fmt.Fprintf(os.Stderr, "Creating Deployment %s...\n", appName)

	deployment := api.Deployment{
		DeploymentRef: api.DeploymentRef{
			AppName: appName,
		},
		ImageUri: imageName,
		Port:     port,
		Expose:   expose,
		Environ:  environmentVariablesApi,
		Volumes:  volumes,
	}

	// Encode the struct as JSON
	deploymentJson, err := json.Marshal(deployment)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest(http.MethodPost,
		server+constants.DeployEndpoint,
		bytes.NewBuffer(deploymentJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	// Debug
	log.Println("req", req)
	log.Println("deployment", deployment)

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	details := api.DeploymentDetails{}
	payload, err := api.ParsePayload(resp, details)
	if err != nil {
		panic(err)
	}

	return payload
}

var deployCmd = &cobra.Command{
	Use:   "deploy",
	Short: "Deploy applications to Galvanize Cloud",
	Long:  `Deploy applications to Galvanize Cloud`,
	PreRun: func(cmd *cobra.Command, args []string) {
		if token == "" {
			cmd.Root().MarkPersistentFlagRequired("token")
		}
	},
	RunE: func(cmd *cobra.Command, args []string) error {

		// Debug
		log.Print("----\n")
		log.Println("App: ", appName)
		log.Println("Image: ", imageName)
		log.Println("Port: ", port)
		log.Println("Expose: ", expose)
		log.Println("Delete: ", deleteDeployment)
		log.Println("Token", token)
		log.Print("----\n")

		if imageName == "" && !deleteDeployment {
			return errors.New("either --image or --delete is required")
		}

		// envPattern := regexp.MustCompile(`^\w+\=[\w\?\:\/\.\=\;\\]+$`)
		for _, environ := range environmentVariables {
			log.Println("Environment variable: ", environ)

			kvPair := strings.SplitN(environ, "=", 2)

			environmentVariablesApi = append(
				environmentVariablesApi,
				api.EnvironmentVariable{
					Key:   kvPair[0],
					Value: kvPair[1],
				})
		}

		match, _ := regexp.MatchString(
			`^[a-zA-Z0-9]+(\-[a-zA-Z0-9]+)*$`, appName)
		if !match {
			log.Fatal(
				`Application name must contain characters a-z, A-Z, 0-9,
						and non-leading or trailing hyphen`)
		}

		auth.PrintIdentity(token)

		op := color.YellowString(fmt.Sprintf("Deploying %s...", appName))
		fmt.Fprintln(os.Stderr, op)

		if deleteDeployment {
			deleteDeploymentFn().Print()
		} else {
			createDeploymentFn().Print()
		}

		return nil
	},
}
