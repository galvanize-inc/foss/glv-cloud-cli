package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/api"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/auth"
	constants "gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/constants"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

func init() {

	logsCmd.Flags().StringVarP(&appName, "app", "a", "",
		"Name of application.")
	logsCmd.MarkFlagRequired("app")

	rootCmd.AddCommand(logsCmd)
}

func getLogs() api.IApi {

	deploymentRef := api.DeploymentRef{
		AppName: appName,
	}

	// Encode the struct as JSON
	deploymentJson, err := json.Marshal(deploymentRef)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest(http.MethodPost,
		server+constants.LogsEndpoint, bytes.NewBuffer(deploymentJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	logs := api.LogsApi{}

	payload, err := api.ParsePayload(resp, logs)
	if err != nil {
		panic(err)
	}

	return payload
}

var logsCmd = &cobra.Command{
	Use:   "logs",
	Short: "get logs for application on Galvanize Cloud",
	Long:  `get logs for application on Galvanize Cloud`,
	Run: func(cmd *cobra.Command, args []string) {

		auth.PrintIdentity(token)

		fmt.Fprintln(os.Stderr, color.YellowString("Getting logs..."))
		logs := getLogs()
		logs.Print()
	},
}
