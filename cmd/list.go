package cmd

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/api"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/auth"
	constants "gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/constants"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(listCmd)
}

func getList() api.IApi {
	// Define request
	req, err := http.NewRequest(http.MethodGet,
		server+constants.ListEndpoint, nil)
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	list := api.ListApi{}

	payload, err := api.ParsePayload(resp, list)
	if err != nil {
		panic(err)
	}

	return payload
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "list applications on Galvanize Cloud",
	Long:  `list applications on Galvanize Cloud`,
	PreRun: func(cmd *cobra.Command, args []string) {
		if token == "" {
			cmd.Root().MarkPersistentFlagRequired("token")
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		auth.PrintIdentity(token)
		serverLabel := color.MagentaString("server:")
		fmt.Fprintf(os.Stderr, "%s %s\n", serverLabel, server)
		fmt.Fprintln(os.Stderr, color.YellowString("Listing Applications..."))
		list := getList()

		list.Print()
	},
}
