package cmd

import (
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/constants"
)

var (
	// Used for flags.
	cfgFile string
	token   string
	server  string
	appName string
	verbose bool

	rootCmd = &cobra.Command{
		Use:   "glv-cloud-cli",
		Short: "glv-cloud-cli manages your Galvanize Cloud environment",
		Long: `A command line interface to Deploy, Delete, and List
		applications running in your teams environment.
		Documentation at: https://gitlab.com/sjp19-public-resources/glv-cloud-cli/-/wikis/home`,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	server = constants.GlvCloudApiPath

	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.glv.yaml)")
	rootCmd.PersistentFlags().StringVarP(&token, "token", "t", "", "token for glv-cloud")
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Verbosity")

	viper.BindPFlag("author", rootCmd.PersistentFlags().Lookup("author"))
	viper.BindPFlag("useViper", rootCmd.PersistentFlags().Lookup("viper"))
	viper.SetDefault("author", "Galvanize Inc. <foss@galvanize.com>")
	viper.SetDefault("license", "MIT")
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cobra" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".glv")
		if err := viper.ReadInConfig(); err != nil {
			if _, ok := err.(viper.ConfigFileNotFoundError); ok {
				// Config file not found
				log.Println("not logged in")
				err = viper.SafeWriteConfig()
				if err != nil {
					panic(err)
				}
			} else {
				// Config file was found but another error was produced
				log.Println("Config file is corrupt. Please correct permissions or remove ~/.glv.yaml")
			}
		}

		// Don't overwrite token
		if viper.GetString("token") != "" {
			token = viper.GetString("token")
		}

		// Don't overwrite token
		if viper.GetString("server") != "" {
			server = viper.GetString("server")
		}

	}

	viper.AutomaticEnv()

	viper.ReadInConfig()

	if verbose {
		log.SetOutput(os.Stdout)
	} else {
		log.SetOutput(io.Discard)
	}
}
