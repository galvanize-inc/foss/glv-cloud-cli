package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/auth"
)

func init() {
	rootCmd.AddCommand(certCmd)
}

var certCmd = &cobra.Command{
	Use:   "cert",
	Short: "Create a self-signed certificate",
	Long:  `Create a self-signed certificate to login to Galvanize Auth`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("create a ca certificate...")
		auth.CreateCertificate()
	},
}
