package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/api"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/auth"
	constants "gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/constants"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

func init() {

	resetCmd.Flags().StringVarP(&appName, "app", "a", "",
		"Name of application.")
	resetCmd.MarkFlagRequired("app")

	rootCmd.AddCommand(resetCmd)
}

func resetDeployment() api.IApi {

	deploymentRef := api.DeploymentRef{
		AppName: appName,
	}

	// Encode the struct as JSON
	deploymentJson, err := json.Marshal(deploymentRef)
	if err != nil {
		panic(err)
	}

	// Define request
	req, err := http.NewRequest(http.MethodPost,
		server+constants.ResetEndpoint, bytes.NewBuffer(deploymentJson))
	if err != nil {
		panic(err)
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read response
	defer resp.Body.Close()

	// body, _ := io.ReadAll(resp.Body)
	// fmt.Printf("debug reset resp body: %s\nstatus: %d\n", body, resp.StatusCode)
	msg := api.Message{}
	payload, err := api.ParsePayload(resp, msg)
	if err != nil {
		panic(err)
	}

	return payload
}

var resetCmd = &cobra.Command{
	Use:   "reset",
	Short: "Reset application on Galvanize Cloud",
	Long:  `Reset application on Galvanize Cloud`,
	Run: func(cmd *cobra.Command, args []string) {

		auth.PrintIdentity(token)

		op := color.YellowString(fmt.Sprintf("Resetting %s...", appName))
		fmt.Fprintln(os.Stderr, op)
		message := resetDeployment()
		message.Print()
	},
}
