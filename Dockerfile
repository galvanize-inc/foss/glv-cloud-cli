# syntax=docker/dockerfile:1

FROM golang:1.21-alpine3.19 AS GO_BUILDER

WORKDIR /glv-cloud-server

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download

ADD . .

# Build
RUN GOOS=linux go build -o /bin/glv-cloud-cli .

FROM alpine:3.18

COPY --from=GO_BUILDER /bin/glv-cloud-cli /bin/glv-cloud-cli

# ENTRYPOINT ["/bin/glv-cloud-cli"]

RUN touch ~/.glv.yaml

CMD /bin/sh
