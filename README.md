# Galvanize Cloud CLI

This tool is a command line interface to manage applications
deployed to Galvanize Cloud.

## How to use

Run the following command:

```sh
$ docker pull registry.gitlab.com/galvanize-inc/foss/glv-cloud-cli
$ docker tag registry.gitlab.com/galvanize-inc/foss/glv-cloud-cli glv-cloud-cli
$ docker run -it glv-cloud-cli
$ ./glv-cloud-cli -h
A command line interface to Deploy, Delete, and List
    applications running in your teams environment.
    Documentation at: https://gitlab.com/sjp19-public-resources/glv-cloud-cli/-/wikis/home

Usage:
  glv-cloud-cli [command]

Available Commands:
  cert        Create a self-signed certificate
  completion  Generate the autocompletion script for the specified shell
  deploy      Deploy applications to Galvanize Cloud
  help        Help about any command
  list        list applications on Galvanize Cloud
  login       login to Galvanize Cloud
  logs        get logs for application on Galvanize Cloud
  reset       Reset application on Galvanize Cloud

Flags:
  -c, --config string   config file (default is $HOME/.glv.yaml)
  -h, --help            help for glv-cloud-cli
  -t, --token string    token for glv-cloud
  -v, --verbose         Verbosity

Use "glv-cloud-cli [command] --help" for more information about a command.
```

## Dot File Configure

A dot file is stored in your home folder called `.glv.yaml`.

## Updates

Breaking changes may require updating to the latest version.

v1.7.0
---

The CLI now generates its own local certificate to
login with a browser. Before, the installer called openssl.
This will reduce errors for many users. A new `cert` command
is now available to create a certificate, but login will call
cert if no certificate is available. Elevated permissions may
be required.

v2.0.0
---

**Update required** the latest version of the
server will not work with prior versions of the CLI. Update
to this version to continue using glv-cloud-cli.

Error formatting is improved to aid users in
troubleshooting common problems. Formatting for results is
colorized with yaml formatted output for ease of reading.
Deployments sourced from gitlab.galvanize.com will require
developer level access to the image repository.

## Shell Completion

This CLI tool has the ability to autocomplete commands in your terminal. The steps will change depending on your operating system. Typically, when you hit the `tab` key after partially typing a command, the autocomplete feature will fill in the rest of the woord or cycle through available opptions. This is not an exhaustive tutorial on autocompletion, but these steps should allow you to load the completion scripts into your shell.

### Windows

After installing the CLI, run the following commands:

```sh
 .\glv-cloud-cli.exe completion powershell > \User\YourName\path\to\completion_script.ps1
```

Then, add the following line to your profile.

```sh
PS> code -r $profile
```

This will source the compltion script. Note the period at the beginning.

```sh
. \User\YourName\path\to\completion_script.ps1
```

### macOS

After installing the CLI, run the following commands:

```sh
echo "source <(glv-cloud-cli completion zsh)" >> ~/.zshrc
```
