package main

import (
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/cmd"
)

func main() {
	cmd.Execute()
}
