package constants

const (
	GlvCloudUri       string = "https://glv-cloud.mod3projects.com"
	GlvCloudApiPrefix string = "/api/v2"
	GlvCloudApiPath   string = GlvCloudUri + GlvCloudApiPrefix
	DeployEndpoint    string = "/deploy"
	ListEndpoint      string = "/list"
	RegistryEndpoint  string = "/registry"
	LogsEndpoint      string = "/logs"
	ResetEndpoint     string = "/reset"
	Version           string = "v2"
)
