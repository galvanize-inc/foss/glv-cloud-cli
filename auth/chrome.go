package auth

import (
	"fmt"
	"os"
	"os/exec"
)

func installCertificateInSystem(installCmdArgs []string) error {
	// Run the command to install the certificate for Chrome
	cmd := exec.Command(installCmdArgs[0], installCmdArgs[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("error installing certificate: %w", err)
	}

	return nil
}
