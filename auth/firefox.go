package auth

import (
	"fmt"
	"os"
	"path/filepath"
)

func installCertificateInFirefox(certDBPath string, certData []byte) error {
	// Check if Firefox is installed
	if _, err := os.Stat(certDBPath); os.IsNotExist(err) {
		return nil
	}

	// List all profile directories
	profiles, err := os.ReadDir(certDBPath)
	if err != nil {
		return fmt.Errorf("error listing profiles: %w", err)
	}

	for _, profile := range profiles {
		// Set the security.enterprise_roots.enabled preference to true
		profileUserJs := filepath.Join(ProfilesDir, profile.Name(), "user.js")
		content := `user_pref("security.enterprise_roots.enabled", true);` + "\n"
		addSetting(profileUserJs, content)
	}

	fmt.Println("Note: Exit Firefox before loggin in so setting can apply.")

	return nil
}
