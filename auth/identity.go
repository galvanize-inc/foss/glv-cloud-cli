package auth

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/fatih/color"
	"gitlab.com/galvanize-inc/foss/glv-cloud-cli/v2/constants"
)

type UserAttributes struct {
	FirstName string          `json:"first_name"`
	LastName  string          `json:"last_name"`
	Email     string          `json:"email"`
	Roles     []string        `json:"roles"`
	Extra     json.RawMessage `json:"-"`
}

type UserData struct {
	Id            string          `json:"id"`
	Type          string          `json:"type"`
	Attributes    UserAttributes  `json:"attributes"`
	Relationships json.RawMessage `json:"relationships"`
}

type UserPayload struct {
	Data UserData `json:"data"`
}

func Identity(authHeader string) (name string, email string, err error) {
	// Define request
	req, err := http.NewRequest(http.MethodGet,
		"https://auth.galvanize.com/api/v1/me", nil)
	if err != nil {
		return
	}

	// Set Content-Type header
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", authHeader))

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	// Read response
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return
	}

	// Read response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	user := UserPayload{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		return
	}

	name = user.Data.Attributes.FirstName + " " + user.Data.Attributes.LastName
	email = user.Data.Attributes.Email
	return
}

func PrintIdentity(token string) {

	name, email, err := Identity(token)
	if err != nil {
		log.Fatalf("%s", err)
	}

	welcome := color.MagentaString("Welcome to Galvanize Cirrus ☁️:")
	email = color.GreenString(email)
	versionHeader := color.MagentaString("Version:")
	fmt.Fprintf(os.Stderr, "%s %s (%s)\n%s %s\n", welcome, name, email, versionHeader, constants.Version)
}
