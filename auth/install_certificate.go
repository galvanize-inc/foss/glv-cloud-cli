package auth

import (
	"fmt"
	"io"
	"os"
	"os/user"
)

// Install certificate for Firefox (if installed) and Chrome
func InstallCertificate() error {
	// Read the certificate file
	certFile, err := os.Open(CertFilePath)
	if err != nil {
		return fmt.Errorf("error opening certificate file: %w", err)
	}
	defer certFile.Close()

	certData, err := io.ReadAll(certFile)
	if err != nil {
		return fmt.Errorf("error reading certificate file: %w", err)
	}

	// Get the current user
	usr, err := user.Current()
	if err != nil {
		return fmt.Errorf("error getting user's home directory: %w", err)
	}

	// Get certificate paths and installation commands
	certDBPath, installCmdArgs, err := getPathsAndCommands(usr)
	if err != nil {
		return fmt.Errorf("error getting certificate paths and installation commands: %w", err)
	}

	// Install certificate in Firefox (if installed) and Chrome
	if err := installCertificateInFirefox(certDBPath, certData); err != nil {
		fmt.Println("Error installing certificate in Firefox:", err)
	}

	if err := installCertificateInSystem(installCmdArgs); err != nil {
		fmt.Println("Error installing certificate in Chrome:", err)
	}

	return nil
}
