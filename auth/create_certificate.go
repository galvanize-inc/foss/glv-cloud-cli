package auth

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"time"
)

func CreateCertificate() {
	log.Println("Creating a new certificate...")
	var input string

	if !IsElevated {
		fmt.Println("Unsufficient privilege detected.\nTry anyways? (Y/n)")

		fmt.Scanln(&input)

		switch input {
		case "", "y", "Y":
			// yes
		default:
			return
		}
	}

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)

	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("Failed to generate serial number: %v", err)
	}

	cert := &x509.Certificate{
		SerialNumber:   serialNumber,
		EmailAddresses: []string{"foss@galvanize.com"},
		Subject: pkix.Name{
			CommonName:    "Galvanize Cirrus",
			Organization:  []string{"Galvanize Inc."},
			Country:       []string{"US"},
			Province:      []string{"CO"},
			Locality:      []string{"Denver"},
			StreetAddress: []string{"1635 Platte Street"},
			PostalCode:    []string{"80202"},
		},
		IPAddresses: []net.IP{net.IPv4(127, 0, 0, 1), net.IPv6loopback},
		NotBefore:   time.Now(),
		NotAfter:    time.Now().AddDate(10, 0, 0),
		DNSNames:    []string{"localhost"},
		PolicyIdentifiers: []asn1.ObjectIdentifier{
			[]int{1, 2, 3, 4},
		},
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		log.Fatalf("Failed to generate key: %v", err)
	}

	caBytes, err := x509.CreateCertificate(
		rand.Reader,
		cert,
		cert,
		&caPrivKey.PublicKey,
		caPrivKey)
	if err != nil {
		log.Fatalf("Failed to generate certificate: %v", err)
	}

	caPEM := new(bytes.Buffer)
	pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})

	log.Printf("Writing certificate: %s", CertFilePath)
	err = os.WriteFile(CertFilePath, caPEM.Bytes(), 0644)
	if err != nil {
		log.Fatalf("Failed to write certificate: %v", err)
	}

	caPrivKeyPEM := new(bytes.Buffer)
	pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(caPrivKey),
	})

	log.Printf("Writing key: %s", CertKeyPath)
	err = os.WriteFile(CertKeyPath, caPrivKeyPEM.Bytes(), 0600)
	if err != nil {
		log.Fatalf("Failed to write key: %v", err)
	}

	log.Println("Installing the new cert...")
	err = InstallCertificate()
	if err != nil {
		log.Fatalf("Failed to install certificate: %v", err)
	}
}
