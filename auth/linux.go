//go:build linux
// +build linux

package auth

import (
	"os"
	"path/filepath"
)

func init() {

	if os.Geteuid() == 0 {
		IsElevated = true
	} else {
		IsElevated = false
	}

	CertFilePath = "/usr/local/galvanize/localhost.crt"
	CertKeyPath = "/usr/local/galvanize/localhost.key"
	FirefoxPath = "firefox"

	// Get the user's home directory
	homeDir := os.Getenv("HOME")

	// Append the Firefox profiles subdirectory to the home directory
	ProfilesDir = filepath.Join(homeDir, ".mozilla", "firefox", "profiles")

	safeDirectoryCheck()

}
