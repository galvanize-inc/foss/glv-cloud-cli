//go:build windows
// +build windows

package auth

import (
	"log"
	"os"
	"path/filepath"

	"golang.org/x/sys/windows"
)

func isUserAnAdmin() bool {

	var Sid *windows.SID

	// https://docs.microsoft.com/en-us/windows/desktop/api/securitybaseapi/nf-securitybaseapi-checktokenmembership
	err := windows.AllocateAndInitializeSid(
		&windows.SECURITY_NT_AUTHORITY,
		2,
		windows.SECURITY_BUILTIN_DOMAIN_RID,
		windows.DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&Sid)
	if err != nil {
		log.Fatalf("SID Error: %s", err)
		return false
	}

	token := windows.Token(0)

	memberp, err := token.IsMember(Sid)
	if err != nil {
		log.Fatalf("Token Membership Error: %s", err)
		return false
	}

	elevatedp := token.IsElevated()

	return memberp || elevatedp
}

func init() {
	CertFilePath = `C:\Program Files\Galvanize\localhost.crt`
	CertKeyPath = `C:\Program Files\Galvanize\localhost.key`
	FirefoxPath = `C:\Program Files\Mozilla Firefox\firefox.exe`
	appdata := os.Getenv("APPDATA")
	ProfilesDir = filepath.Join(appdata, "Mozilla", "Firefox", "Profiles")
	IsElevated = isUserAnAdmin()

	safeDirectoryCheck()
}
