package auth

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/user"
	"runtime"
	"strings"

	"path/filepath"
)

var (
	CertFilePath string
	CertKeyPath  string
	IsElevated   bool
	FirefoxPath  string
	ProfilesDir  string
)

/* There are more inits that are conditionally built to define the variables
 * above. If this was built for a different OS, more work needs to be done.
 */
func init() {
	switch runtime.GOOS {
	case "darwin", "linux", "windows":
		// suported
	default:
		fmt.Println("Unsupported operating system")
	}
}

func safeDirectoryCheck() {
	// Get the directory path
	dir := filepath.Dir(CertFilePath)

	// Check if the directory exists
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		// Create directories if they don't exist
		err := os.MkdirAll(dir, 0755)
		if err != nil {
			log.Fatalf("error creating directories: %v", err)
		}
	}
}

func getPathsAndCommands(usr *user.User) (string, []string, error) {
	var (
		// cert9.db path for Firefox
		certDBPath string

		// Command and arguments for installing certificate on Chrome/Firefox
		installCmdArgs []string
	)

	switch runtime.GOOS {
	case "darwin":
		// macOS paths
		certDBPath = filepath.
			Join(usr.HomeDir,
				"Library",
				"Application Support",
				"Firefox",
				"Profiles")
		installCmdArgs = []string{
			"sudo",
			"security",
			"add-trusted-cert",
			"-d",
			"-r",
			"trustRoot",
			"-k",
			"/Library/Keychains/System.keychain",
			CertFilePath}
	case "windows":
		// Windows paths
		certDBPath = filepath.
			Join(usr.HomeDir, "AppData", "Roaming", "Mozilla", "Firefox", "Profiles")
		installCmdArgs = []string{"certutil", "-addstore", "-user", "Root", CertFilePath}
	case "linux":
		// Linux paths
		distroID, err := getLinuxDistroID()
		if err != nil {
			return "", nil, fmt.
				Errorf("error getting Linux distribution ID: %w", err)
		}

		switch distroID {
		case "debian", "ubuntu":
			certDBPath = "/etc/ca-certificates/trust-source/anchors"
			installCmdArgs = []string{
				"sudo",
				"update-ca-certificates",
				CertFilePath}
		case "fedora":
			certDBPath = "/etc/pki/ca-trust/source/anchors"
			installCmdArgs = []string{
				"sudo",
				"update-ca-trust",
				"extract", "-f",
				CertFilePath}
		case "arch":
			certDBPath = filepath.
				Join(usr.HomeDir, ".mozilla", "firefox", "Profiles")
			installCmdArgs = []string{"sudo", "trust", "anchor", "-d", CertFilePath}
		default:
			return "", nil, errors.New("unsupported Linux distribution")
		}
	default:
		return "", nil, errors.New("unsupported operating system")
	}

	return certDBPath, installCmdArgs, nil
}

func getLinuxDistroID() (string, error) {
	data, err := os.ReadFile("/etc/os-release")
	if err != nil {
		return "", fmt.Errorf("error reading /etc/os-release: %w", err)
	}

	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		parts := strings.SplitN(line, "=", 2)
		if len(parts) == 2 && parts[0] == "ID" {
			return strings.Trim(parts[1], `"`), nil
		}
	}

	return "", errors.New("distribution ID not found")
}
