package auth

import (
	"fmt"
	"os"
)

func addSetting(fileName string, content string) {

	// Check if the file exists
	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		// Create the file if it doesn't exist
		file, err := os.Create(fileName)
		if err != nil {
			fmt.Println("Error creating file:", err)
			return
		}
		defer file.Close()
	}

	// Read the file content
	fileContent, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	// Check if the content already exists in the file
	if !stringInSlice(content, string(fileContent)) {
		// Open the file in append mode
		file, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Println("Error opening file:", err)
			return
		}
		defer file.Close()

		// Write the content to the file
		_, err = file.WriteString(content)
		if err != nil {
			fmt.Println("Error writing to file:", err)
			return
		}

		fmt.Println("Content added to the file.")
	} else {
		fmt.Println("Content already exists in the file.")
	}
}

func stringInSlice(str string, list string) bool {
	return !(len(list) < len(str)) && list[len(list)-len(str):] == str
}
