//go:build darwin
// +build darwin

package auth

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
)

func init() {

	if os.Geteuid() == 0 {
		IsElevated = true
	} else {
		IsElevated = false
	}

	CertFilePath = "/usr/local/galvanize/localhost.crt"
	CertKeyPath = "/usr/local/galvanize/localhost.key"
	FirefoxPath = "/Applications/Firefox.app/Contents/MacOS/firefox"

	// Get the current user's information
	currentUser, err := user.Current()
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	// Get the user's home directory
	homeDir := currentUser.HomeDir

	ProfilesDir = filepath.Join(homeDir, "Library", "Application Support", "Firefox", "Profiles")

	safeDirectoryCheck()

}
